#!/usr/bin/env sh

exit_error() {
  echo "ERROR: ${*}" 1>&2
  exit
}

header() {
  echo
  echo '========================================'
  echo "${*}"
}

root_dir="$(cd "$(dirname "${0}")/../" && pwd)"
cd "${root_dir}" || exit_error "Cannot change into ${root_dir}"

PATH="${root_dir}/node_modules/.bin:${PATH}"
PATH="${HOME}/.local/share/gem/bin:${PATH}"
export PATH

header CSS
stylelint --config ./config/stylelintrc.json --formatter verbose ./source/assets/css/

header HTML
html-linter --config ./config/html-linter.json

header Javascript
eslint --config ./config/eslintrc.yml ./source/assets/js/

header Markdown
MDL_OPTS="$(tr "\n" " " <./config/mdl.opts)"
export MDL_OPTS
# shellcheck disable=SC2086
mdl ${MDL_OPTS} .

header Shell
export SHELLCHECK_OPTS='--norc --format=tty --shell=sh'
shellcheck bin/*.sh
