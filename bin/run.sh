#!/usr/bin/env sh

exit_error() {
  echo "ERROR: ${*}" 1>&2
  exit
}

root_dir="$(cd "$(dirname "${0}")/../" && pwd)"
cd "${root_dir}" || exit_error "Cannot change into ${root_dir}"

ruby -run -e httpd public/ -p 8080
