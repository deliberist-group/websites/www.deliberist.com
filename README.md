# README.md

[![pipeline status](https://gitlab.com/deliberist-group/websites/www.deliberist.com/badges/main/pipeline.svg)](https://gitlab.com/deliberist-group/websites/www.deliberist.com/-/commits/main)

The source code for [www.deliberist.com](https://www.deliberist.com/).  Rather
than explain what deliberist is, what it means, and how to contribute, please
check out the site!

## Running the Site Locally

```bash
# Terminal 1
./bin/build.sh

# Terminal 2
./bin/run.sh
```

## Useful Links

- [GitLab Groups API](https://docs.gitlab.com/ee/api/groups.html)

- [GitLab Issues API](https://docs.gitlab.com/ee/api/boards.html)

- [GitLab Pages, setting up subdomain DNS records](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html#for-subdomains)
