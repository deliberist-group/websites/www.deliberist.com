include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

variables:
  IMAGE_ALPINE: "alpine:latest"
  IMAGE_BASH: "koalaman/shellcheck-alpine:v0.7.1"
  IMAGE_NODEJS: "node:14.4-alpine3.12"
  IMAGE_RUBY: "ruby:2.7-alpine3.11"

stages:
  - lint
  - test    # Needed for the included templates.
  - deploy

.before_script: &before_script |
  env | sort

css:
  stage: lint
  image: "${IMAGE_NODEJS}"
  before_script:
    - *before_script
    - npm install --global stylelint stylelint-config-standard
    - stylelint --version
  script:
    - stylelint --config ./config/stylelintrc.json --formatter verbose ./source/assets/css/

html:
  image: "${IMAGE_NODEJS}"
  stage: lint
  before_script:
    - *before_script
    - npm install --global html-linter
    - html-linter --version
  script:
    - html-linter --config ./config/html-linter.json

javascript:
  image: "${IMAGE_NODEJS}"
  stage: lint
  before_script:
    - *before_script
    - npm install --global eslint
    - eslint --version
  script:
    - eslint --config ./config/eslintrc.yml ./source/assets/js/

markdown:
  image: "${IMAGE_RUBY}"
  stage: lint
  before_script:
    - export MDL_OPTS="$(tr "\n" " " <./config/mdl.opts)"
    - *before_script
    - gem install mdl
    - mdl --version
  script:
    - mdl ${MDL_OPTS} .

lint_shell:
  image: "${IMAGE_BASH}"
  stage: lint
  before_script:
    - *before_script
    - apk add file
    - shellcheck --version
  script:
    - export SHELLCHECK_OPTS='--norc --format=tty --shell=sh'
    - shellcheck bin/*.sh

pages:
  image: "${IMAGE_RUBY}"
  stage: deploy
  variables:
    CONFIG_GLCI: "./config/_config-glci.yml"
    HOMEPAGE_URL: "https://www.deliberist.com/"
    JEKYLL_ENV: "production"
  artifacts:
    expire_in: 42 years
    paths:
      - public/
  only:
    - main
  before_script:
    - *before_script
    - echo '>>>> CREATING CONFIG WITH NON-MAIN BRANCH PROPERTIES <<<<'
    - touch "${CONFIG_GLCI}"
    - echo -e "baseurl\x3A ${PUBLIC_URL}" >>"${CONFIG_GLCI}"
    - echo -e "deliberist\x3A" >>"${CONFIG_GLCI}"
    - echo -e "  revision\x3A ${CI_COMMIT_SHA}" >>"${CONFIG_GLCI}"
    - cat "${CONFIG_GLCI}"
    - echo '>>> Installing build tools required to compile native Ruby dependencies. <<<'
    - apk add g++ gcc make musl-dev
    - echo '>>> Installing Javascript runtime. <<<'
    - apk add nodejs
    - echo '>>> Installing Ruby dependencies. <<<'
    - gem --version
    - gem install bundle
    - bundler --version
    - bundler update
  script:
    - bundler exec jekyll build --config "./config/_config.yml,${CONFIG_GLCI}"
    - echo "Deploying ${HOMEPAGE_URL}"

review_pages:
  image: "${IMAGE_RUBY}"
  stage: deploy
  variables:
    CONFIG_GLCI: "./config/_config-glci.yml"
    HOMEPAGE_URL: "${CI_JOB_URL}/artifacts/file/public/index.html"
    PUBLIC_URL: "/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public"
  artifacts:
    expire_in: 1 week
    paths:
      - public/
  environment:
    auto_stop_in: 1 week
    name: review/${CI_COMMIT_REF_NAME}
    url: "${HOMEPAGE_URL}"
  except:
    - main
  before_script:
    - *before_script
    - echo '>>>> CREATING CONFIG WITH NON-MAIN BRANCH PROPERTIES <<<<'
    - touch "${CONFIG_GLCI}"
    - echo -e "baseurl\x3A ${PUBLIC_URL}" >>"${CONFIG_GLCI}"
    - echo -e "deliberist\x3A" >>"${CONFIG_GLCI}"
    - echo -e "  revision\x3A ${CI_COMMIT_SHA}" >>"${CONFIG_GLCI}"
    - cat "${CONFIG_GLCI}"
    - echo '>>> Installing build tools required to compile native Ruby dependencies. <<<'
    - apk add g++ gcc make musl-dev
    - echo '>>> Installing Javascript runtime. <<<'
    - apk add nodejs
    - echo '>>> Installing Ruby dependencies. <<<'
    - gem --version
    - gem install bundle
    - bundler --version
    - bundler update
  script:
    - bundler exec jekyll build --config "./config/_config.yml,${CONFIG_GLCI}"
    - echo "Deploying source review to ${HOMEPAGE_URL}"
