"use strict";

// const ESE_GROUP_ID = "7625073";
const DELIBERIST_GROUP_ID = "6816200";

const getGroupsProjectsListUrl =
    (groupId) => `https://gitlab.com/api/v4/groups/${groupId}/projects`;

// const printObjectInPromiseChain = (obj) => {
//     console.log(obj);
//     return obj;
// };

const shuffleInPlace = (array) => {
    for (let index = array.length - 1; index > 0; index -= 1) {
        const newIndex = Math.floor(Math.random() * (index + 1));
        const temp = array[index];
        array[index] = array[newIndex];
        array[newIndex] = temp;
    }
    return array;
};

const filterGroupMetadata = (groupMetadata) => {
    const allowed = [
        "description",
        "name",
        "web_url",
        "avatar_url",
        "forks_count",
        "star_count",
        "open_issues_count"
    ];
    const newGroupMetadata = [];
    groupMetadata.forEach((projectMetadata) => {
        const newProjectMetadata = Object
            .keys(projectMetadata)
            .filter((key) => allowed.includes(key))
            .reduce((obj, key) => {
                obj[key] = projectMetadata[key];
                return obj;
            }, {});
        newGroupMetadata.push(newProjectMetadata);
    });
    return newGroupMetadata;
};

const processMarkdown = (groupMetadata) => {
    /* global showdown */
    const converter = new showdown.Converter({
        "disableForced4SpacesIndentedSublists": true,
        "encodeEmails": true,
        "excludeTrailingPunctuationFromURLs": true,
        "ghCodeBlocks": true,
        "ghCompatibleHeaderId": true,
        "ghMentions": true,
        "ghMentionsLink": "https://gitlab.com/{u}",
        "literalMidWordUnderscores": true,
        "noHeaderId": false,
        "omitExtraWLInCodeBlocks": true,
        "parseImgDimensions": true,
        "prefixHeaderId": true,
        "requireSpaceBeforeHeadingText": true,
        "simpleLineBreaks": true,
        "simplifiedAutoLink": true,
        "smartIndentationFix": true,
        "smoothLivePreview": false,
        "strikethrough": true,
        "tables": true,
        "tablesHeaderId": true,
        "tasklists": true
    });
    groupMetadata.forEach((projectMetadata) => {
        projectMetadata.description =
            converter.makeHtml(projectMetadata.description);
    });
    return groupMetadata;
};

const displayGroupMetadata = (groupMetadata) => {
    // Why clone.getElementsByClassName() not work with the clones? ¯\_(ツ)_/¯
    const projectList = document.querySelector("#projectList");
    const template = document.getElementById("projectTemplate");
    groupMetadata.forEach((pm) => {
        // Variable pm = Project Metadata, shortened because... line lengths...
        const clone = template.content.cloneNode(true);
        clone.querySelector(".project__url").href = pm.web_url;
        clone.querySelector(".project__avatar").src = pm.avatar_url;
        clone.querySelector(".project__name").innerText = pm.name;
        clone.querySelector(".project__stars").innerText = pm.star_count;
        clone.querySelector(".project__forks").innerText = pm.forks_count;
        clone.querySelector(".project__openIssues").innerText =
            pm.open_issues_count;
        clone.querySelector(".project__description").innerHTML = pm.description;
        projectList.appendChild(clone);
    });
    return groupMetadata;
};


const deleteInconvenienceStatement = (_unusedGroupMetadata) => {
    document.querySelector("#projectList__inconvenience").remove();
};

(function gitlabMain () {
    const projectsUrl = getGroupsProjectsListUrl(DELIBERIST_GROUP_ID);
    fetch(projectsUrl, {"method": "GET"})
        // Intersperse the printObjectInPromiseChain() when debugging!
        // .then(printObjectInPromiseChain)
        .then((response) => response.json())
        .then(filterGroupMetadata)
        .then(processMarkdown)
        .then(shuffleInPlace)
        .then(displayGroupMetadata)
        .then(deleteInconvenienceStatement)
        .catch((error) => {
            console.error(error);
        });
}());
